import { CurrencyCodePipe } from './currency-code.pipe';

describe('CurrencyCodePipe', () => {
  it('create an instance', () => {
    const pipe = new CurrencyCodePipe();
    expect(pipe).toBeTruthy();
  });
  it('should be currency code', () => {
    const pipe = new CurrencyCodePipe();
    expect(pipe.transform(1)).toEqual('USD')
  });  
});
