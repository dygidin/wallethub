import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersListComponent } from './customers-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MyCurrencyPipe } from '../currency-pipe';
import { CurrencyCodePipe } from '../currency-code.pipe';
import { By } from '@angular/platform-browser';


describe('CustomersListComponent', () => {
  let component: CustomersListComponent;
  let fixture: ComponentFixture<CustomersListComponent>;
  let list;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomersListComponent, MyCurrencyPipe, CurrencyCodePipe],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersListComponent);
    component = fixture.componentInstance;
    list = fixture.debugElement.query(By.css('ul')).nativeElement;

    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be a list', () => {
    component.list = [{
      id: 1, name: 'test', amount: 1000, currency: 1, email:'test@mail.ru'
    },
    {
      id: 2, name: 'test2', amount: 500, currency: 2, email:'test2@mail.ru'
    }];
    fixture.detectChanges();
    expect(list.querySelectorAll('li').length).toEqual(2)
  });

});
