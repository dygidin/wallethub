import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomerData } from '../types/types';


@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {
  @Input() activeItem:CustomerData;
  @Input() list: Array<CustomerData> = [];
  @Output() onSelect: EventEmitter<CustomerData> = new EventEmitter();
  @Output() onDelete: EventEmitter<CustomerData> = new EventEmitter();
  
  constructor() { }

  select(item: CustomerData) {
    this.onSelect.emit(item);
  }

  delete(item: CustomerData) {
    this.onDelete.emit(item);
  }

  ngOnInit() {

  }

}
