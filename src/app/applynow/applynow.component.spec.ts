import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { ApplynowComponent } from './applynow.component';
import { MyCurrencyPipe } from '../currency-pipe';
import { MyModalComponent, MyModalService } from '../mymodal.component/mymodal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CurrencyCodePipe } from '../currency-code.pipe';
import { FormsModule, NgForm } from '@angular/forms';
import { CurrencyFormatDirective } from '../currency-format.directive';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';


describe('ApplynowComponent', () => {
  let component: ApplynowComponent;
  let fixture: ComponentFixture<ApplynowComponent>;
  let elementEmail: HTMLInputElement;
  let elementAmount: HTMLInputElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [ApplynowComponent, MyCurrencyPipe, MyModalComponent, CurrencyCodePipe, CurrencyFormatDirective],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      providers: [MyModalService, MyCurrencyPipe, NgForm]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplynowComponent);
    component = fixture.componentInstance;
    elementEmail = fixture.debugElement.query(By.css('input[name="customerEmail"]')).nativeElement;
    elementAmount = fixture.debugElement.query(By.css('input[name="customerAmount"]')).nativeElement;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check email format', (done) => {
    component.formDataModal.onOpen.subscribe(() => {
      elementEmail.value = 'denis_gidin';
      elementEmail.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(elementEmail.classList.contains('ng-invalid')).toBe(true);
      done();
    })
  });

  it('should check amount format (only numbers)', (done) => {
    component.formDataModal.onOpen.subscribe(() => {
      elementAmount.value = 'hi';            
      elementAmount.dispatchEvent(new Event('blur'));
      fixture.detectChanges();     
      expect(elementAmount.value === '').toBe(true);
      done();
    })
  });
});
