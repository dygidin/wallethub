import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ApplynowRoutingModule } from './applynow-routing.module';
import { ApplynowComponent } from './applynow.component';
import { MyModalComponent, MyModalService } from '../mymodal.component/mymodal.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ApplynowComponent, MyModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ApplynowRoutingModule, 
    SharedModule
  ],
  providers: [MyModalService]
})
export class ApplynowModule { }
