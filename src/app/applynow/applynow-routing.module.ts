import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplynowComponent } from './applynow.component';

const routes: Routes = [{ path: '', component: ApplynowComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplynowRoutingModule { }
