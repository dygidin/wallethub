import { Component, OnInit, ViewChild } from '@angular/core';
import { MyModalComponent } from '../mymodal.component/mymodal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerData, Currencies } from '../types/types';
import { AppService } from '../app.service';


@Component({
  selector: 'app-applynow',
  templateUrl: './applynow.component.html',
  styleUrls: ['./applynow.component.css']
})
export class ApplynowComponent implements OnInit {
  @ViewChild('formDataModal', {
    static: true
  }) formDataModal: MyModalComponent;

  customerData: CustomerData = new CustomerData();
  currencies = Currencies;
  currencyCode:string = 'USD';
  keys: any[];

  constructor(private _router: Router, private _appService: AppService, private route: ActivatedRoute, ) {    
    this.keys = Object.keys(this.currencies).filter(c => !isNaN(Number(c)));
    route.params.subscribe(value => {
      if (value && value.id) {
        const item: CustomerData = this._appService.customersList.find(x => x.id == value.id);
        if (item) {
          this.customerData = Object.assign({}, item);
          this.currencyCode = this.currencies[this.customerData.currency];
          this._appService.onCustomerDataChange.emit(item);          
        }
      }
    })
  }

  onSelect(event) {
    this.customerData.currency = event.target.value;
    this.currencyCode = this.currencies[event.target.value];
  }

  submit() {
    this._appService.setCustomerData(this.customerData);
    this.formDataModal.close();
  }

  ngOnInit() {
    this.formDataModal.title = 'Enter information (Template form)';
    this.formDataModal.open();
    this.formDataModal.onClose.subscribe(() => {
      this._router.navigate(['/']);
    })
  }

  ngOnDestroy() {
    this.formDataModal.close();
  }

}
