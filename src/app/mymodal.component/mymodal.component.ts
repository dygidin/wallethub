import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit, Injectable
} from '@angular/core';

@Injectable()
export class MyModalService {
  public modalList = [];
  public modalCount: number = 0;
  addModal(id) {
    this.modalList.push(id);
    this.modalCount = this.modalList.length;
  }

  delModal(id) {
    this.modalList.splice(this.modalList.indexOf(id), 1);
    this.modalCount = this.modalList.length;
  }

  nextModal(id) {
    let index = this.modalList.indexOf(id);
    if (index >= 0 && index < this.modalList.length - 1)
      return this.modalList[index + 1];
    else return false;
  }
  
  constructor() {}
}

@Component({
  selector: 'my-modal',
  templateUrl: './mymodal.componant.html',
  styleUrls: ['./mymodal.component.css']
})

export class MyModalComponent implements OnInit {
  @Input() title: string;
  @Input() large: boolean = false;
  @Input() footer: boolean = false;
  @Output() onOpen = new EventEmitter();
  @Output() onClose = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  id = new Date().getTime().toString();
  show: boolean = false;
  fadeIn: boolean = false;
  constructor(private _modalService: MyModalService) {

  }

  open() {
    this.show = true;
    this._modalService.addModal(this.id);
    setTimeout(() => {
      this.fadeIn = true;
      this.onShowEvn();
    }, 200);
  }

  close() {
    this._modalService.delModal(this.id);
    if (this._modalService.modalCount == 0)
      document.body.style.overflow = "auto";

    setTimeout(() => {
      this.fadeIn = false;
      setTimeout(() => {
        this.show = false;
        this.onCloseEvn();
      }, 100);
    }, 100);

  }

  submit() {
    this.onSubmit.emit(this);
    this.close();
  }

  keyPress(event) {
    if (event.keyCode == 27 && event.target.getAttribute("id") == this.id) {
      this.close();
    }
  }

  onShowEvn() {
    this.onOpen.emit(this);
  }

  onCloseEvn() {
    this.onClose.emit(this);
  }

  ngOnInit() {}
}