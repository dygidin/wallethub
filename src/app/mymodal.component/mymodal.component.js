"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var MyModalService = (function () {
    function MyModalService() {
        this.modalList = [];
        this.modalCount = 0;
    }
    MyModalService.prototype.addModal = function (id) {
        this.modalList.push(id);
        this.modalCount = this.modalList.length;
    };
    MyModalService.prototype.delModal = function (id) {
        this.modalList.splice(this.modalList.indexOf(id), 1);
        this.modalCount = this.modalList.length;
    };
    MyModalService.prototype.nextModal = function (id) {
        var index = this.modalList.indexOf(id);
        if (index >= 0 && index < this.modalList.length - 1)
            return this.modalList[index + 1];
        else
            return false;
    };
    MyModalService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], MyModalService);
    return MyModalService;
}());
exports.MyModalService = MyModalService;
var MyModalComponent = (function () {
    function MyModalComponent(_modalService) {
        this._modalService = _modalService;
        this.large = false;
        this.footer = false;
        this.onOpen = new core_1.EventEmitter();
        this.onClose = new core_1.EventEmitter();
        this.onSubmit = new core_1.EventEmitter();
        this.id = new Date().getTime().toString();
        this.show = false;
        this.fadeIn = false;
        this.fadeInDown = false;
        this.fadeOutUp = false;
    }
    MyModalComponent.prototype.open = function () {
        var _this = this;
        this.show = true;
        this.fadeIn = true;
        this.fadeOutUp = false;
        this._modalService.addModal(this.id);
        document.body.style.overflow = "hidden";
        setTimeout(function () {
            _this.fadeInDown = true;
            document.getElementById(_this.id).focus();
            _this.onShowEvn();
        }, 200);
    };
    MyModalComponent.prototype.close = function () {
        var _this = this;
        this.fadeOutUp = true;
        this._modalService.delModal(this.id);
        if (this._modalService.modalCount == 0)
            document.body.style.overflow = "auto";
        setTimeout(function () {
            _this.fadeIn = false;
            setTimeout(function () {
                _this.fadeInDown = false;
                _this.show = false;
                _this.onCloseEvn();
            }, 100);
        }, 100);
    };
    MyModalComponent.prototype.closeBg = function (event) {
        if (event.target.getAttribute("parentid") == this.id) {
            this.close();
        }
    };
    MyModalComponent.prototype.submit = function () {
        this.onSubmit.emit(this);
        this.close();
    };
    MyModalComponent.prototype.keyPress = function (event) {
        if (event.keyCode == 27 && event.target.getAttribute("id") == this.id) {
            this.close();
        }
    };
    MyModalComponent.prototype.onShowEvn = function () {
        this.onOpen.emit(this);
    };
    MyModalComponent.prototype.onCloseEvn = function () {
        this.onClose.emit(this);
    };
    MyModalComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], MyModalComponent.prototype, "title", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], MyModalComponent.prototype, "large", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], MyModalComponent.prototype, "footer", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MyModalComponent.prototype, "onOpen", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MyModalComponent.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MyModalComponent.prototype, "onSubmit", void 0);
    MyModalComponent = __decorate([
        core_1.Component({
            selector: 'my-modal',
            template: "\n  <div class=\"my-modal\" [attr.id]=\"id\" [hidden]=\"show==false\" tabIndex=\"1\" (keydown)=\"keyPress($event)\">\n    <div class=\"background animate\" [attr.parentid]=\"id\" [class.fadeIn]=\"fadeIn==true\" [class.fadeOut]=\"fadeIn==false\" (click)=\"closeBg($event)\">\n      <div class=\"my-modal-content animate\" [hidden]=\"fadeInDown==false\" [class.lg]=\"large==true\" [class.fadeInDown]=\"fadeInDown==true\" [class.fadeOutUp]=\"fadeOutUp==true\">\n        <div class=\"my-modal-header\">\n          <h4>{{ title }}</h4>\n          <button type=\"button\" class=\"close\" (click)=\"close()\"><span aria-hidden=\"true\">\u00D7</span>\n          </button>        \n        </div>\n        <div class=\"my-modal-body\">\n          <ng-content></ng-content>          \n        </div>\n        <hr *ngIf=\"footer==true\" />\n        <div class=\"my-modal-footer\" *ngIf=\"footer==true\">\n          <button class=\"btn btn-success\" (click)=\"submit()\">\u0412\u044B\u0431\u0440\u0430\u0442\u044C</button>\n          <button class=\"btn btn-default\" (click)=\"close()\">\u041E\u0442\u043C\u0435\u043D\u0430</button>        \n        </div>\n      </div>\n    </div>"
        }), 
        __metadata('design:paramtypes', [MyModalService])
    ], MyModalComponent);
    return MyModalComponent;
}());
exports.MyModalComponent = MyModalComponent;
//# sourceMappingURL=mymodal.component.js.map