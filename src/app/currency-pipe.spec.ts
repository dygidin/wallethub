import { MyCurrencyPipe } from './currency-pipe';

describe('CurrencyPipe', () => {
  it('create an instance', () => {
    const pipe = new MyCurrencyPipe();
    expect(pipe).toBeTruthy();
  });

  it('should be format value for USD', () => {
    const pipe = new MyCurrencyPipe();
    expect(pipe.transform('1234', 2, 'USD')).toEqual('$1,234.00')
  });

  it('should be format value for EUR', () => {
    const pipe = new MyCurrencyPipe();
    expect(pipe.transform('1234', 2, 'EUR')).toEqual('€1.234,00')
  });  
});
