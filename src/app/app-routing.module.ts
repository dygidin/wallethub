import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    component: HomeComponent,
    data: { animation: 'Home' },
    children: [
      {
        path: 'apply-now',
        loadChildren: () => import('./applynow/applynow.module').then(m => m.ApplynowModule)
      },
      {
        path: 'apply-now/:id',
        loadChildren: () => import('./applynow/applynow.module').then(m => m.ApplynowModule)
      }
    ]
  },
  {
    path: 'add-balance',
    data: { animation: 'Section' },
    loadChildren: () => import('./add-balance/add-balance.module').then(m => m.AddBalanceModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
