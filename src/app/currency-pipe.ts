import { Pipe, PipeTransform } from '@angular/core';

const PADDING = "000000";
 interface ICurrencyConfig {
  name: string;
  prefix: string;
  decimal_seperator: string;
  thousand_seperator: string;
  suffix: string;
  regexp: RegExp;
}

export class Currencies {
  list: Array<ICurrencyConfig> = [{
    name: 'USD',
    prefix: '',
    decimal_seperator: ".",
    thousand_seperator: ",",
    suffix: '$',
    regexp: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g)
  },
  {
    name: 'EUR',
    prefix: '',
    decimal_seperator: ",",
    thousand_seperator: ".",
    suffix: '€',
    regexp: new RegExp(/^[0-9]+(\,[0-9]*){0,1}$/g)
  }];

  getConfig(name: string = 'USD'): ICurrencyConfig {
    return this.list.filter((item) => item.name == name)[0];
  }
}

@Pipe({
  name: 'currencyPipe'
})
export class MyCurrencyPipe implements PipeTransform {
  private currencyConfig: ICurrencyConfig = new Currencies().getConfig('USD');

  constructor() {

  }

  configCurrency(currencyCode: string = 'USD') {
    this.currencyConfig = new Currencies().getConfig(currencyCode);
  }

  transform(value: string, fractionSize: number = 2, currencyCode = 'USD', prevCurrencyCode: string = 'USD'): string {
    if (value == '0' || value == '') return value;
    if (isNaN(parseFloat(value))) return null;
    this.configCurrency(currencyCode);
    let dec: string = this.currencyConfig.decimal_seperator;

    if (prevCurrencyCode != currencyCode) {
      dec = new Currencies().getConfig(prevCurrencyCode).decimal_seperator;
    }

    let [integer, fraction = ""] = (value || "").toString()
      .split(dec);

    fraction = fractionSize > 0
      ? this.currencyConfig.decimal_seperator + (fraction + PADDING).substring(0, fractionSize)
      : "";

    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.currencyConfig.thousand_seperator);

    return this.currencyConfig.suffix + this.currencyConfig.prefix + integer + fraction;
  }

  parse(value: string, fractionSize: number = 2, currencyCode = 'USD'): string {
    this.configCurrency(currencyCode);
    let [integer, fraction = ""] = (value || "").replace(this.currencyConfig.prefix, "")
      .replace(this.currencyConfig.suffix, "")
      .split(this.currencyConfig.decimal_seperator);

    integer = integer.replace(new RegExp('\\' + this.currencyConfig.thousand_seperator, "g"), "");

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this.currencyConfig.decimal_seperator + (fraction + PADDING).substring(0, fractionSize)
      : "";
    return integer + fraction;
  }

}
