import { Pipe, PipeTransform } from '@angular/core';
import { Currencies } from './types/types';

@Pipe({
  name: 'currencyCode'
})
export class CurrencyCodePipe implements PipeTransform {

  transform(value: any): any {
    return Currencies[value];
  }

}
