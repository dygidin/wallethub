import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { HomeComponent } from './home.component';
import { MyCurrencyPipe } from '../currency-pipe';
import { MyModalComponent } from '../mymodal.component/mymodal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CurrencyCodePipe } from '../currency-code.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomerData } from '../types/types';
import { By } from '@angular/platform-browser';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let userDiv:HTMLDivElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ HomeComponent, MyCurrencyPipe, MyModalComponent, CurrencyCodePipe ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;   
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show user data', (done) => {       
    component.customerData = new CustomerData('User Test', 1234, 1, 'denis_gidin@mail.ru');        
    fixture.detectChanges();    
    userDiv = fixture.debugElement.query(By.css('.customer-data')).nativeElement;    
    expect(userDiv.innerHTML.indexOf('User Test')!==-1).toBe(true);
    done();
  });  
});
