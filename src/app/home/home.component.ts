import { Component, OnInit, Input } from '@angular/core';
import { CustomerData } from '../types/types';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @Input() customerData: CustomerData;  
  subscriber: Subscription;

  constructor(public _appService: AppService, private _router: Router) {
    this.subscriber = this._appService.onCustomerDataChange.subscribe((customerData: CustomerData) => {
      this.customerData = customerData;
      this._appService.addCustomer(customerData);      
    })
  }

  add() {   
    this._router.navigate(['/home/apply-now']);
  }

  edit() {   
    this._router.navigate(['/home/apply-now', { id: this.customerData.id }]);
  }

  onSelectCustomer(customerData: CustomerData) {
    this.customerData = customerData;    
  }

  onDeleteCustomer(customerData: CustomerData) {
    this._appService.deleteCustomer(customerData);
    if (customerData == this.customerData)
      this.customerData = null;
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    if (this.subscriber)
      this.subscriber.unsubscribe();
  }

}
