import { Directive, HostListener, ElementRef, Input, SimpleChanges } from '@angular/core';
import { MyCurrencyPipe, Currencies } from './currency-pipe';

@Directive({
  selector: '[appCurrencyFormat]'
})
export class CurrencyFormatDirective {
  @Input('appCurrencyFormat') currencyCode: string;
  // Allow decimal numbers. The \. is only allowed once to occur
  private regex: RegExp = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);
  // Allow key codes for special events. Reflect :
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Delete'];
  private el: any;
  constructor(
    private elementRef: ElementRef,
    private currencyPipe: MyCurrencyPipe
  ) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnInit() {
    this.el.value = this.currencyPipe.transform(this.el.value, 2, this.currencyCode);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.el.value = this.currencyPipe.transform(this.el.value, 2, this.currencyCode);
    }, 100);
  }

  @HostListener("focus", ["$event.target.value"])
  onFocus(value) {
    this.el.value = this.currencyPipe.parse(value, 2, this.currencyCode); // opossite of transform    
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    this.el.value = this.currencyPipe.transform(value, 2, this.currencyCode, this.currencyCode);
  }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }

    let current: string = this.el.value;
    let next: string = current.concat(event.key);
    this.regex = new Currencies().getConfig(this.currencyCode).regexp;

    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['currencyCode']) {
      this.currencyCode = changes['currencyCode']['currentValue'];
      this.el.value = this.currencyPipe.parse(this.el.value, 2, changes['currencyCode']['previousValue']);
      this.el.value = this.currencyPipe.transform(this.el.value, 2, this.currencyCode, changes['currencyCode']['previousValue']);
    }
  }

}
