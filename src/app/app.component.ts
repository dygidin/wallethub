import { Component } from '@angular/core';
import { ILink } from './types/types';
import { slideInAnimation } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ slideInAnimation ]
})
export class AppComponent {
  title = 'wallethub';
  menuList: Array<ILink> = [
    {
      name: 'Home',
      link: 'home'
    }, {
      name: 'Apply Now',
      link: 'home/apply-now'
    },
    {
      name: 'Add balance',
      link: 'add-balance'
    }];

    constructor() {}
}
