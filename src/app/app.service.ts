import { Injectable, EventEmitter } from '@angular/core';
import { CustomerData } from './types/types';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  customersList: Array<CustomerData> = [];
  onCustomerDataChange: EventEmitter<CustomerData> = new EventEmitter();
  setCustomerData(customerData: CustomerData) {
    this.onCustomerDataChange.emit(customerData);
  }

  addCustomer(customerData: CustomerData) {
    const findIndex = this.customersList.findIndex(x => x.name === customerData.name);
    if (findIndex == -1)
      this.customersList.push(customerData);
    else
      this.customersList[findIndex] = customerData;
    this.save();  
  }

  deleteCustomer(customedData:CustomerData) {
    const findIndex = this.customersList.findIndex(x => x.name === customedData.name);
    this.customersList.splice(findIndex, 1);
    this.save();
  }

  save() {
    localStorage.setItem('customers', JSON.stringify(this.customersList));
  }

  load() {
    const json: string = localStorage.getItem('customers');
    if (json) {
      this.customersList = JSON.parse(json);
    }
  }

  constructor() {
    this.load();
  }
}
