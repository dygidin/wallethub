import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBalanceComponent } from './add-balance.component';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MyCurrencyPipe } from '../currency-pipe';
import { CurrencyCodePipe } from '../currency-code.pipe';
import { CurrencyFormatDirective } from '../currency-format.directive';
import { By } from '@angular/platform-browser';

describe('AddBalanceComponent', () => {
  let component: AddBalanceComponent;
  let fixture: ComponentFixture<AddBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [AddBalanceComponent, MyCurrencyPipe, CurrencyCodePipe, CurrencyFormatDirective],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      providers: [MyCurrencyPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add and delete amound item', () => {
    component.addAmount();
    fixture.detectChanges();
    let list = fixture.debugElement.query(By.css('.amounts')).nativeElement;
    expect(list.querySelectorAll('.list').length == 1).toBe(true);
    component.deleteAmount(0);
    fixture.detectChanges();
    expect(list.querySelectorAll('.list').length == 0).toBe(true);
  });
});
