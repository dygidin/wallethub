import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-add-balance',
  templateUrl: './add-balance.component.html',
  styleUrls: ['./add-balance.component.css']
})
export class AddBalanceComponent implements OnInit {
  currencyCode:string = 'USD';
  formDataJson:string;
  addBalanceForm = this.fb.group({
    name: ['', Validators.required],    
    amounts: this.fb.array([
      
    ])
  });

  get amounts() {
    return this.addBalanceForm.get('amounts') as FormArray;
  }

  addAmount() {
    this.amounts.push(this.fb.control('', Validators.required));
  }

  deleteAmount(i:number) {
    this.amounts.removeAt(i)
  }

  onSubmit() {    
   this.formDataJson = JSON.stringify(this.addBalanceForm.value);
  }

  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
  }

}
