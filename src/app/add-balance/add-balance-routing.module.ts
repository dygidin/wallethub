import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddBalanceComponent } from './add-balance.component';

const routes: Routes = [{ path: '', component: AddBalanceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddBalanceRoutingModule { }
