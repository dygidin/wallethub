import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBalanceRoutingModule } from './add-balance-routing.module';
import { AddBalanceComponent } from './add-balance.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [AddBalanceComponent],
  imports: [
    CommonModule,
    AddBalanceRoutingModule, ReactiveFormsModule, SharedModule
  ]
})
export class AddBalanceModule { }
