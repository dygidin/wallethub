import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyCurrencyPipe } from '../currency-pipe';
import { CurrencyCodePipe } from '../currency-code.pipe';
import { CurrencyFormatDirective } from '../currency-format.directive';


@NgModule({
  declarations: [CurrencyCodePipe, MyCurrencyPipe, CurrencyFormatDirective],
  imports: [
    CommonModule
  ],
  exports: [CurrencyCodePipe, MyCurrencyPipe, CurrencyFormatDirective],
  providers: [MyCurrencyPipe],
})
export class SharedModule { }
