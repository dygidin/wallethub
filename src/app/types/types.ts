export interface ILink {
  name: string;
  link: string;
}

export enum Currencies {
  USD = 1,
  EUR = 2
}

export class CustomerData {
  id: number = (new Date()).getTime();
  constructor(public name: string = '', public amount: number = null, public currency: Currencies = Currencies.USD, public email: string = '') {

  }
}